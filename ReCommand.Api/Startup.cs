using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Consul;
using Luke.Infrastructure.Consul;
using Luke.Infrastructure.Consul.Options;
using Luke.Infrastructure.DotNetCoreCap;
using Luke.Infrastructure.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ReCommand.Api.EfData;
using ReCommand.Api.Service;
using Resilience;

namespace ReCommand.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddDbContext<ReCommandDbContext>(builder =>
            {

                builder.UseMySql(Configuration.GetConnectionString("UserMysqlLocal"), x =>
                {
                    x.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                });
            });

            services.AddOptions();
            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.Configure<UseCapDiscoveryOptions>(Configuration.GetSection("DotNetCapDiscovery"));

            services.AddIdentityServer()
        .AddDeveloperSigningCredential()
        .AddInMemoryClients(Config.GetClients())
       .AddInMemoryIdentityResources(Config.GetIdentityResources());

            services.AddMyDnsQuery();
            services.AddMyConsulClient();
            services.AddMyHttpClient();

            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<IUserService, UserService>();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "http://localhost:97";
                options.Audience = "gateway_recommandapi";
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;

            });

            services.AddMyDotNetCoreCap(options =>
            {
                options.UseEntityFramework<ReCommandDbContext>();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime,
          IOptions<ServiceDisvoveryOptions> serviceOptions, IConsulClient consulClient)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMyConsul(applicationLifetime, serviceOptions, consulClient);
            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
