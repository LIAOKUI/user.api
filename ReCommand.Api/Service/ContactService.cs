﻿using DnsClient;
using Luke.Infrastructure.Consul.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ReCommand.Api.Dtos;
using Resilience;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReCommand.Api.Service
{
    public class ContactService: IContactService
    {
        private readonly IHttpClient httpClient;
        private readonly ILogger<ContactService> logger;
        private readonly string contactServiceUrl;
        public ContactService(IHttpClient httpClient, ILogger<ContactService> logger,
            IOptions<ServiceDisvoveryOptions> options, IDnsQuery dnsQuery)
        {
            this.httpClient = httpClient;
            this.logger = logger;
            var result = dnsQuery.ResolveService("service.consul", options.Value.UserServiceName);
            var addressList = result.FirstOrDefault().AddressList;
            var address = addressList.Any() ? addressList.FirstOrDefault().ToString() : result.FirstOrDefault().HostName;
            var port = result.FirstOrDefault().Port;
            contactServiceUrl = $"HTTP://{address}:{port}";
        }
        /// <summary>
        /// 获取联系人列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<Contact>> GetContactListByUserIdAsync(int userId)
        {
            List<Contact> contacts = null;
            try
            {
                var reponse = await httpClient.GetStringAsync($"{contactServiceUrl}/api/contact/{userId}");
                if (!string.IsNullOrWhiteSpace(reponse))
                {
                    contacts = JsonConvert.DeserializeObject<List<Dtos.Contact>>(reponse);
                }
            }
            catch(Exception ex)
            {
                logger.LogError(ex, "调用http服务GetContactListByUserIdAsync 请求发生错误");
            }
            return contacts;
        }
    }
}
