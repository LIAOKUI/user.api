﻿using DnsClient;
using Luke.Infrastructure.Consul.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ReCommand.Api.Dtos;
using Resilience;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReCommand.Api.Service
{
    public class UserService : IUserService
    {
        private readonly IHttpClient httpClient;
        private readonly ILogger<UserService> logger;
        private readonly string userServiceUrl;
        public UserService(IHttpClient httpClient, ILogger<UserService> logger,
            IOptions<ServiceDisvoveryOptions> options, IDnsQuery dnsQuery)
        {
            this.httpClient = httpClient;
            this.logger = logger;
            var result = dnsQuery.ResolveService("service.consul", options.Value.UserServiceName);
            var addressList = result.FirstOrDefault().AddressList;
            var address = addressList.Any() ? addressList.FirstOrDefault().ToString() : result.FirstOrDefault().HostName;
            var port = result.FirstOrDefault().Port;
            userServiceUrl = $"HTTP://{address}:{port}";
        }
        /// <summary>
        /// 获取用户信息
        /// </summary>
        public async Task<BaseUserInfo> GetBaseUserInfoAsync(int userId)
        {
            BaseUserInfo result = null;

            try
            {
                var response = await httpClient.GetStringAsync($"{userServiceUrl}/api/users/baseinfo/{userId}");
             if(!string.IsNullOrWhiteSpace(response))
                {
                    result = JsonConvert.DeserializeObject<BaseUserInfo>(response);
                }
            }
            catch (Exception ex)
            {

                logger.LogError(ex, "调用http服务GetBaseUserInfoAsync 请求发生错误");
            }
            return result;
        }
    }
}
