﻿using Microsoft.EntityFrameworkCore;
using ReCommand.Api.EfData.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReCommand.Api.EfData
{
    public class ReCommandDbContext : DbContext
    {
        public ReCommandDbContext( DbContextOptions<ReCommandDbContext> options) : base(options)
        {
        }
        public DbSet<ProjectReCommand> ProjectReCommands { get; set; }
        public DbSet<ProjectReferenceUser> ProjectReferenceUsers { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectReCommand>().ToTable("ProjectReCommands")
                .HasKey(x => x.Id);

            modelBuilder.Entity<ProjectReferenceUser>().ToTable("ProjectReferenceUsers")
                .HasKey(x => x.Id);

            base.OnModelCreating(modelBuilder);
        }
    }
}
