﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReCommand.Api.IntergationEvents
{
    public class ProjectCreatedIntergrationEvent : IntegrationEvent
    {
        public int UserId { get; set; }
        public string Company { get; set; }
        public int ProjectId { get; set; }
        public string Introduction { get; set; }
    }
}
