﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReCommand.Api.EfData;

namespace ReCommand.Api.Controllers
{
    public class RecommandController : BaseController
    {
        private readonly ReCommandDbContext dbContext;
        public RecommandController(ReCommandDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet("projects")]
        public async Task<IActionResult> Get()
        {
            var projectReCommands = await dbContext.ProjectReCommands.Include
                (x => x.ProjectReferenceUsers).Where(s => s.UserId == UserIdentity.UserId).ToListAsync();
            return Ok(projectReCommands);
        }
    }
}