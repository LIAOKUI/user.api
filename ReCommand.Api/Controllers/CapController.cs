﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using ReCommand.Api.EfData;
using ReCommand.Api.EfData.Entities;
using ReCommand.Api.IntergationEvents;
using ReCommand.Api.Service;

namespace ReCommand.Api.Controllers
{
    public class CapController : BaseController
    {
        private readonly ReCommandDbContext dbContext;
        private readonly IUserService userService;
        private readonly IContactService contactService;
        public CapController(ReCommandDbContext dbContext,
            IUserService userService,
            IContactService contactService
            )
        {
            this.dbContext = dbContext;
            this.userService = userService;
            this.contactService = contactService;
        }

        [CapSubscribe("projectapi.projectcreated")]
        [NonAction]
        public async Task Process(ProjectCreatedIntergrationEvent @event)
        {
            var info = await userService.GetBaseUserInfoAsync(@event.UserId);
            var contacts = await contactService.GetContactListByUserIdAsync(@event.UserId);
            if (info != null || contacts != null)
            {
                List<ProjectReCommand> projectReCommands = new List<ProjectReCommand>();
                foreach (var contact in contacts)
                {
                    var projectRecommand = new ProjectReCommand()
                    {
                        Introduction = @event.Introduction,
                        UserId = contact.UserId,
                        Company = @event.Company,
                        CreateTime = @event.CreationDate,
                        EnumReCommandType = EnumReCommandType.Friend,
                        FromUserAvator = info.Avatar,
                        FromUserId = @event.UserId,
                        ReCommandTime = DateTime.Now,
                        ProjectId = @event.ProjectId,
                        FromUserName = info.Name,
                    };
                    projectReCommands.Add(projectRecommand);
                }
                await dbContext.ProjectReCommands.AddRangeAsync(projectReCommands);
            }
            await dbContext.SaveChangesAsync();
        }
    }
}