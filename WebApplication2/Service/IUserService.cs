﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Dtos;

namespace WebApplication2.Service
{
    public interface IUserService
    {
        Task<UserInfo> CheckOrCreate(string phone);
    }
}
