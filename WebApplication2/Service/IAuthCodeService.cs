﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Service
{
    public interface IAuthCodeService
    {
        /// <summary>
        /// 根据手机号和验证码验证
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="authCode"></param>
        /// <returns></returns>
        bool Validate(string phone, string authCode);
    }
}
