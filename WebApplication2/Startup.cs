using IdentityServer4.Services;
using Luke.Infrastructure.Identity;
using Luke.Infrastructure.Identity.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IdentityModel.Tokens.Jwt;

namespace WebApplication2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

             services.AddTransient<IProfileService, ProfileService>();

            services.AddIdentityServer()
               // .AddExtensionGrantValidator<SmsAuthCodeValidator>()
                .AddDeveloperSigningCredential()
                .AddInMemoryClients(Config.GetClients())
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
              //  .AddInMemoryApiResources(Config.GetApiResources())
               // .AddProfileService<ProfileService>()
                ;

            services.AddOptions();
          //  services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));

            //services.AddSingleton<IDnsQuery>(p =>
            //{
            //    var serviceConfiguration = p.GetRequiredService<IOptions<ServiceDisvoveryOptions>>().Value;

            //    return new LookupClient(IPAddress.Parse(serviceConfiguration.Consul.DnsEndpoint.Address), serviceConfiguration.Consul.DnsEndpoint.Port);

            //});

            //services.AddSingleton(new HttpClient());

            //services.AddSingleton<ResilienceHttpClientFactory>(sp =>
            //{
            //    var loggger = sp.GetRequiredService<ILogger<ResilienceHttpClientFactory>>();
            //    var logggerHttpClinet = sp.GetRequiredService<ILogger<ResilientHttpClient>>();
            //    var httpContextAccessor = sp.GetRequiredService<IHttpContextAccessor>();
            //    var retryCount = 5;
            //    var expCountAllowedBeforeBreak = 5;
            //    return new ResilienceHttpClientFactory(logggerHttpClinet, httpContextAccessor, retryCount, expCountAllowedBeforeBreak);
            //});

            //services.AddSingleton<IHttpClient>(sp =>
            //{
            //    var resilienceClientFactory = sp.GetRequiredService<ResilienceHttpClientFactory>();
            //    return resilienceClientFactory.GetResilienceHttpClient();
            //});
            //services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IAuthCodeService, TestAuthCodeService>();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.Authority = "http://localhost:95";
                    options.Audience = "gateway_contactapi";
                    options.RequireHttpsMetadata = false;
                  
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();

            app.UseAuthorization();
            app.UseIdentityServer();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
