﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<UserIdentity> Get()
        {
            var user = new UserIdentity();

            user.Avatar = User.Claims.First(x => x.Type == "avatar").Value;
            user.Company = User.Claims.First(x => x.Type == "company").Value;
            user.Name = User.Claims.First(x => x.Type == "name").Value;

            return user;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class UserIdentity
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Titile { get; set; }
        public string Company { get; set; }
    }
}
