using FluentAssertions;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Api.Models;
using Xunit;

namespace User.Api.Unit
{
    public class UerControllerUnitTests
    {
        private UserDbContext GetUserContext()
        {
            var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            var userContext = new UserDbContext(options);
            userContext.Users.Add(new Models.Users() { Id = 1, Name = "test" });
            userContext.SaveChanges();
            return userContext;
        }
        private (Controllers.UserController controller, UserDbContext userDbContext) GetUserController()
        {
            var context = GetUserContext();
            var loggerMoq = new Mock<ILogger<Controllers.UserController>>();
            var logger = loggerMoq.Object;
            return (new Controllers.UserController(context, logger), userDbContext: context);
        }

        [Fact]
        public async Task Patch_RturnNewName_WithExpectedAddNewNameParameter()
        {
            (Controllers.UserController controller, UserDbContext userDbContext) = GetUserController();

            var document = new JsonPatchDocument<Users>();
            document.Replace(s => s.Name, "lei");

            var response = await controller.Patch(document);
            var result = response.Should().BeOfType<JsonResult>().Subject;

            var appUser = result.Value.Should().BeAssignableTo<Users>().Subject;
            appUser.Id.Should().Be(1);
            appUser.Name.Should().Be("lei");

            var userModel = await userDbContext.Users.SingleOrDefaultAsync(s => s.Id == 1);

            userModel.Should().NotBeNull();
            userModel.Name.Should().Be("lei");
        }
        [Fact]
        public async Task Get_ReturnRigthUser_WithExpectedParameters()
        {
            (Controllers.UserController controller, UserDbContext userDbContext) = GetUserController();
            var response = await controller.Get();

            var result = response.Should().BeOfType<JsonResult>().Subject;
            var appUser = result.Value.Should().BeAssignableTo<Users>().Subject;
            appUser.Id.Should().Be(1);
            appUser.Name.Should().Be("test");
            //  Assert.IsType<JsonResult>(response);
        }

      

        [Fact]
        public async Task Patch_RturnNewName_WithExpectedAddProperty()
        {
            (Controllers.UserController controller, UserDbContext userDbContext) = GetUserController();

            var document = new JsonPatchDocument<Users>();
            document.Replace(s => s.Properties, new List<UserProperty> {
                new UserProperty(){  Key="test", Text="����", Value="����"}
            });

            var response = await controller.Patch(document);
            var result = response.Should().BeOfType<JsonResult>().Subject;

            var appUser = result.Value.Should().BeAssignableTo<Users>().Subject;
            appUser.Properties.Count().Should().Be(2);
            appUser.Properties.First().Value.Should().Be("����") ;
            appUser.Properties.First().Key.Should().Be("test");
            var userModel = await userDbContext.Users.SingleOrDefaultAsync(s => s.Id == 1);

            userModel.Properties.First().Value.Should().Be("����");
            userModel.Properties.First().Key.Should().Be("test");
        }

        [Fact]
        public async Task Patch_RturnNewName_WithExpectedRemoveProperty()
        {
            (Controllers.UserController controller, UserDbContext userDbContext) = GetUserController();

            var document = new JsonPatchDocument<Users>();
            document.Replace(s => s.Properties, new List<UserProperty> {
            });

            var response = await controller.Patch(document);
            var result = response.Should().BeOfType<JsonResult>().Subject;

            var appUser = result.Value.Should().BeAssignableTo<Users>().Subject;
            appUser.Properties.Should().BeEmpty();
            var userModel = await userDbContext.Users.SingleOrDefaultAsync(s => s.Id == 1);
            userModel.Properties.Should().BeEmpty();
           
        }
    }
}
