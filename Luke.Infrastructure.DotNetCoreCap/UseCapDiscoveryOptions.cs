﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luke.Infrastructure.DotNetCoreCap
{
 public    class UseCapDiscoveryOptions
    {
         public string DiscoveryServerHostName { get; set; }
        public int DiscoveryServerPort { get; set; }
        public string CurrentNodeHostName { get; set; }
        public int CurrentNodePort { get; set; }
        public string NodeId { get; set; }
        public string NodeName { get; set; }
    }
}
