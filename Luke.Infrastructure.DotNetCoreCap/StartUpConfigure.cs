﻿using DotNetCore.CAP;
using DotNetCore.CAP.Dashboard.NodeDiscovery;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace Luke.Infrastructure.DotNetCoreCap
{
    public static  class StartUpConfigure
    {
      
        public static void AddMyDotNetCoreCap(this IServiceCollection services,  Action<CapOptions> setupAction)
        {
            var useCapDiscovery = services.BuildServiceProvider().GetRequiredService<IOptions<UseCapDiscoveryOptions>>().Value;

            services.AddCap(options =>
            {
               setupAction.Invoke(options);
                options.UseRabbitMQ((o) => {
                    o.HostName = "localhost";
                    o.Password = "guest";
                    o.UserName = "guest";
                });
                options.UseDashboard();

                options.UseDiscovery(d =>
                {
                    d.DiscoveryServerHostName = useCapDiscovery.DiscoveryServerHostName;
                    d.DiscoveryServerPort = useCapDiscovery.DiscoveryServerPort;
                    d.CurrentNodeHostName = useCapDiscovery.CurrentNodeHostName;
                    d.CurrentNodePort = useCapDiscovery.CurrentNodePort;
                    d.NodeId = useCapDiscovery.NodeId;
                    d.NodeName = useCapDiscovery.NodeName;
                });
                options.SucceedMessageExpiredAfter = 24 * 3600;
                options.FailedRetryCount = 5;
            });
        }
    }
}
