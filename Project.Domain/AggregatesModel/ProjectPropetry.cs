﻿using Project.Domain.SeedWork;
using System.Collections.Generic;

namespace Project.Domain.AggregatesModel
{
    /// <summary>
    /// 项目属性
    /// </summary>
    public   class ProjectPropetry: ValueObject
    {
        public int UserId { get; set; }
        public int ProjecetId { get; set; }
        public string Text { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Key;
            yield return Key;
            yield return Value;
        }
    }
}
