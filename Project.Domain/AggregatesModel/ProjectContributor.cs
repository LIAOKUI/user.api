﻿using Project.Domain.SeedWork;
using System;

namespace Project.Domain.AggregatesModel
{
    /// <summary>
    /// 项目贡献者
    /// </summary>
    public class ProjectContributor : Entity
    {
    
        public int UserId { get; set; }
        public int ProjectId { get; set; }
        public string UserName { get; set; }
        public string Avator { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsColsed { get; set; }
        /// <summary>
        /// 1 财务顾问,2 投资机构
        /// </summary>
        public int ContributorType { get; set; }
    }
}
