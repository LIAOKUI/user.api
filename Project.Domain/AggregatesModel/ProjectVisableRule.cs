﻿using Project.Domain.SeedWork;

namespace Project.Domain.AggregatesModel
{
    /// <summary>
    /// 项目可见规则
    /// </summary>
    public class ProjectVisableRule : Entity
    {
        public int ProjecetId { get; set; }
        /// <summary>
        /// 可见的
        /// </summary>
        public bool Visable { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        public string  Tags { get; set; }
    }
}
