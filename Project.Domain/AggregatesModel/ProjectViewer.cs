﻿using Project.Domain.SeedWork;
using System;

namespace Project.Domain.AggregatesModel
{
    /// <summary>
    /// 项目查看
    /// </summary>
    public class ProjectViewer : Entity
    {

        public int UserId { get; set; }
        public string Avator { get; set; }
        public int ProjectId { get; set; }
        public string UserName { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}
