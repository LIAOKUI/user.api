﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contact.Api.Data;
using Contact.Api.Dto;
using Contact.Api.IntergrationEvents;
using Contact.Api.Models;
using Contact.Api.Service;
using DotNetCore.CAP;
using Luke.Infrastructure.Commom.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Contact.Api.Controllers
{
    public class ContactController : BaseController
    {
        private readonly ILogger<ContactController> _logger;
        private readonly IUserService _userService;

        private readonly IContactApplyRequestRepository _contactApplyRequestRepository;
        private readonly IContactRepository _contactRepository;
        public ContactController(IContactRepository contactRepository, IUserService userService, ILogger<ContactController> logger, IContactApplyRequestRepository contactApplyRequestRepository)
        {
            _contactRepository = contactRepository;
            _userService = userService;
            _logger = logger;
            _contactApplyRequestRepository = contactApplyRequestRepository;
        }
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var result = await _contactRepository.GetContactsAsync(UserIdentity.UserId);
            return Ok(result);
        }

        [HttpPut("tag")]
        public async Task<IActionResult> TagContactAsync([FromBody]TagContactInputViewModel tagContactInput, CancellationToken cancellationToken)
        {
            var result = await _contactRepository.TagContactAsync(UserIdentity.UserId, tagContactInput.ContactId, tagContactInput.Tags, cancellationToken);

            if (result)
                return Ok();


            //TBD 
            return BadRequest();
        }
        /// <summary>
        /// 获取好友申请列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("apply-requests")]
        public async Task<List<ContactApplyRequest>> GetApplyRequestsAsync(int userId, CancellationToken cancellationToken)
        {
            var requests = await _contactApplyRequestRepository.GetRequestAsync(userId, cancellationToken);
            return requests;
        }

        /// <summary>
        /// 添加好友请求
        /// </summary>
        /// <returns></returns>
        [HttpPost("apply-requests")]
        public async Task<IActionResult> AddApplyRequestAsync(int userId, CancellationToken cancellationToken)
        {
            var userBaseUserInfo = await _userService.GetBaseUserInfoAsync(userId);

            if (userBaseUserInfo == null)
            {
                throw new Exception("用户参数错误");
            }
            var result = await _contactApplyRequestRepository.AddRequestAsync(new Models.ContactApplyRequest()
            {
                UserId = userId,
                ApplierId = UserIdentity.UserId,
                Name = userBaseUserInfo.Name,
                Company = userBaseUserInfo.Company,
                Title = userBaseUserInfo.Title,
                CreateTime = DateTime.Now,
                Avatar = userBaseUserInfo.Avatar,
                Approvaled = 0,
                HandledTime = DateTime.Now
            }, cancellationToken);
            if (!result)
                return BadRequest();
            return Ok();
        }
        /// <summary>
        /// 通过好友请求
        /// </summary>
        /// <param name="applierId"></param>
        /// <returns></returns>
        [HttpPut("apply-requests")]
        public async Task<IActionResult> ApprovalApplyRequestAsync(int applierId, CancellationToken cancellationToken)
        {
            var result = await _contactApplyRequestRepository
                .ApprovalAsync(UserIdentity.UserId, applierId, cancellationToken);

            if (!result)
                return BadRequest();

            var applier = await _userService.GetBaseUserInfoAsync(applierId);
            var userInfo = await _userService.GetBaseUserInfoAsync(UserIdentity.UserId);

            await _contactRepository.AddContactAsync(UserIdentity.UserId, applier, cancellationToken);
            await _contactRepository.AddContactAsync(applierId, userInfo, cancellationToken);
            return Ok();
        }

        [NonAction]
        [CapSubscribe("userinfo.changed")]
        public async Task ConsumerUserInfoChangedEvent(AppUserInfoChangedEvent user, CancellationToken cancellationToken)
        {
            var model = MapperHelper<AppUserInfoChangedEvent, BaseUserInfo>.Map(user);
            await _contactRepository.UpdateContactInfoAsync(model, cancellationToken);
        }
    }
}