﻿using Contact.Api.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contact.Api.Controllers
{
    [Route("api/[controller]")]
    public class BaseController : Controller
    {

        /// <summary>
        /// 从token中获取当前请求用户的userid以及基本信息
        /// </summary>
        protected UserIdentity UserIdentity
        {
            get
            {
                var http = HttpContext.User.Claims;
                var claims = User.Claims;
                var identity = new UserIdentity
                {
                    UserId = Convert.ToInt32(claims.FirstOrDefault(s => s.Type == "sub").Value),
                    Name = claims.FirstOrDefault(s => s.Type == "name").Value,
                    Avatar = claims.FirstOrDefault(s => s.Type == "avatar").Value,
                    Titile = claims.FirstOrDefault(s => s.Type == "title").Value,
                    Company = claims.FirstOrDefault(s => s.Type == "company").Value
                };

                return identity;
            }
        }
    }
}
