﻿using Contact.Api.Data.AppSetting;
using Contact.Api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contact.Api.Data
{
    public class ContactContext
    {
        private IMongoDatabase _dataBase;
        private readonly string _logCollection;
        public ContactContext(IOptions<DBSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _dataBase = client.GetDatabase(settings.Value.Database);
            _logCollection = settings.Value.LogCollection;
        }


        private void CheckAddCreateCollection(string collectionName)
        {
            var collectionList = _dataBase.ListCollections().ToList();
            var collectionNames = new List<string>();
            //获得所有集合的名称
            collectionList.ForEach(x => { collectionNames.Add(x["name"].AsString); });
            //如果没有这个集合就创建一个 （相当于创建一个表）
            if (!collectionNames.Contains(collectionName))
            {
                _dataBase.CreateCollection(collectionName);
            }
        }

        public IMongoCollection<LogEventData> LogEventDatas
        {
            get
            {
                CheckAddCreateCollection(_logCollection);
                return _dataBase.GetCollection<LogEventData>(_logCollection);
            }
        }
        public IMongoCollection<ContactBook> ContactBooks
        {
            get
            {
                CheckAddCreateCollection("ContactBooks");
                return _dataBase.GetCollection<ContactBook>("ContactBooks");
            }
        }
        public IMongoCollection<T> GetCollection<T>(string name) where T : class
        {
            CheckAddCreateCollection(name);
            return _dataBase.GetCollection<T>(name);  
        }
        public IMongoCollection<ContactApplyRequest>ContactApplyRequests
        {
            get
            {
                CheckAddCreateCollection("ContactApplyRequests");
                return _dataBase.GetCollection<ContactApplyRequest>("ContactApplyRequests");
            }
        }
    }
}
