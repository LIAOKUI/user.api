﻿using Contact.Api.Data.AppSetting;
using Contact.Api.Dto;
using Contact.Api.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Contact.Api.Data
{
    public class MongoContactRepository : IContactRepository
    {
        private readonly ContactContext _context;

        public MongoContactRepository(IOptions<DBSettings> settings)
        {
            _context = new ContactContext(settings);
        }

        public async Task<bool> AddContactAsync(int userId, BaseUserInfo contact, CancellationToken cancellationToken)
        {
            if (await _context.ContactBooks.CountDocumentsAsync(c => c.UserId == userId) == 0)
            {
                await _context.ContactBooks.InsertOneAsync(new ContactBook() { UserId = userId });
            }
            var filter = Builders<ContactBook>.Filter.Eq(c => c.UserId, userId);
            var update = Builders<ContactBook>.Update.AddToSet(c => c.Contacts, new Models.Contact()
            {
                UserId = contact.UserId,
                Avatar = contact.Avatar,
                Company = contact.Company,
                Name = contact.Name,
                Title = contact.Title
            });
            var result = await _context.ContactBooks.UpdateOneAsync(filter, update, null, cancellationToken);
            return result.MatchedCount == result.ModifiedCount && result.ModifiedCount == 1;
        }

        public async Task<List<Models.Contact>> GetContactsAsync(int userId)
        {
            var contactbook = await (await _context.ContactBooks.FindAsync(c => c.UserId == userId)).FirstOrDefaultAsync(); ;

            if (contactbook != null)
                return contactbook.Contacts;
            return new List<Models.Contact>();
        }

        public async Task<bool> TagContactAsync(int userId, int contactId, List<string> tags, CancellationToken cancellationToken)
        {
            var filter = Builders<ContactBook>.Filter.And(
                Builders<ContactBook>.Filter.Eq(c => c.UserId, userId),

                Builders<ContactBook>.Filter.Eq("Contacts.UserId", contactId)
                );
            var update = Builders<ContactBook>.Update.Set("Contacts.$.Tags", tags);

            var result = await _context.ContactBooks.UpdateOneAsync(filter, update, null, cancellationToken);
            return result.MatchedCount == result.ModifiedCount && result.ModifiedCount == 1;

        }

        /// <summary>
        /// 更新联系人
        /// </summary>
        /// <param name="baseUserInfo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> UpdateContactInfoAsync(BaseUserInfo userInfo, CancellationToken cancellationToken)
        {

            var contactBook = await (await _context.ContactBooks.FindAsync
                (c => c.UserId == userInfo.UserId, null, cancellationToken)).FirstOrDefaultAsync(cancellationToken);
            if (contactBook == null)
            {
                // throw new Exception("");
                return true;
            }
            var contactIds = contactBook.Contacts.Select(c => c.UserId);

            var filter = Builders<ContactBook>.Filter.And(
                Builders<ContactBook>.Filter.In(c => c.UserId, contactIds),

                //为数组字段创建元素匹配筛选器。
                Builders<ContactBook>.Filter.ElemMatch(c => c.Contacts, c => c.UserId == userInfo.UserId)
                );

            var update = Builders<ContactBook>.Update
                .Set("Contacts.$.Name", userInfo.Name)
                     .Set("Contacts.$.Avatar", userInfo.Avatar)
                          .Set("Contacts.$.Company", userInfo.Company)
                               .Set("Contacts.$.Title", userInfo.Title);
            var result = await _context.ContactBooks.UpdateManyAsync(filter, update);
            return result.MatchedCount == result.ModifiedCount && result.ModifiedCount == 1;
        }
    }
}
