﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contact.Api.Data.AppSetting;
using Contact.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace Contact.Api.Data
{
    public class MongoContactApplyRequestRepository : IContactApplyRequestRepository
    {
        private readonly ContactContext _context;

        public MongoContactApplyRequestRepository(IOptions<DBSettings> settings)
        {
            _context = new ContactContext(settings);
        }
        public async Task<bool> AddRequestAsync(ContactApplyRequest request, CancellationToken cancellationToken)
        {
            var filter = Builders<ContactApplyRequest>.Filter.Where(r => r.UserId == request.UserId
            && r.ApplierId == request.ApplierId);

            if (await _context.ContactApplyRequests.CountDocumentsAsync(filter) > 0)
            {
                var update = Builders<ContactApplyRequest>.Update.Set(r => r.ApplyTime, DateTime.Now);

                // var options = new UpdateOptions {IsUpsert=true };

                var result = await _context.ContactApplyRequests.UpdateOneAsync(filter, update, null, cancellationToken);
                return result.MatchedCount == result.ModifiedCount && result.ModifiedCount == 1;
            }
            await _context.ContactApplyRequests.InsertOneAsync(request, null, cancellationToken);
            return true;
        }

        public async Task<bool> ApprovalAsync(int userId, int applierId, CancellationToken cancellationToken)
        {
            var filter = Builders<ContactApplyRequest>.Filter.Where(r => r.UserId == userId
            && r.ApplierId == applierId);
            

            var update = Builders<ContactApplyRequest>.Update.Set(r => r.ApplyTime, DateTime.Now)
                .Set(r =>r.Approvaled,1)
                .Set(r=>r.HandledTime,DateTime.Now);
            var result = await _context.ContactApplyRequests.UpdateOneAsync(filter, update, null, cancellationToken);
            return result.MatchedCount == result.ModifiedCount && result.ModifiedCount == 1;
        }

        public async Task<List<ContactApplyRequest>> GetRequestAsync(int userId, CancellationToken cancellationToken)
        {
            var result = (await _context.ContactApplyRequests.FindAsync(s => s.UserId == userId));
            return result.ToList(cancellationToken);
        }
    }
}
