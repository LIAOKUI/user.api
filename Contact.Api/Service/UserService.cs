﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Contact.Api.Dto;
using DnsClient;
using Luke.Infrastructure.Consul.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Resilience;

namespace Contact.Api.Service
{
    public class UserService : IUserService
    {
        private IHttpClient _httpClient;
        private readonly string _userServiceUrl;
        private readonly ILogger<UserService> _logger;
        public UserService(IHttpClient httpClient, IOptions<ServiceDisvoveryOptions> serviceDisvoveryOptions, IDnsQuery dnsQuery,
             ILogger<UserService> logger)
        {
            _httpClient = httpClient;
            var address = dnsQuery.ResolveService("service.consul", serviceDisvoveryOptions.Value.UserServiceName);
            var addressList = address.First().AddressList;
            var host = addressList.Any() ? addressList.First().ToString().TrimEnd('.') : address.First().HostName.TrimEnd('.');
            var port = address.First().Port;
            _userServiceUrl = $"http://{host}:{port}";
            _logger = logger;
        }
        public async Task<BaseUserInfo> GetBaseUserInfoAsync(int userId)
        {
            try
            {
                var result = await _httpClient.GetStringAsync(_userServiceUrl + "/api/User/baseInfo/" + userId);
                if (string.IsNullOrWhiteSpace(result))
                    return null;
                var userInfo = JsonConvert.DeserializeObject<BaseUserInfo>(result);
                _logger.LogTrace("BaseUserInfo," + result);
                return userInfo;

            }
            catch (Exception ex)
            {
                _logger.LogError("CheckOrCreate 再重试之后失败," + ex.Message + ex.StackTrace);
                throw ex;
            }

        }
    }
}
