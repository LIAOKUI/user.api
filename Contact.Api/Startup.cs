using Consul;
using Contact.Api.Data;
using Contact.Api.Data.AppSetting;
using Contact.Api.Models;
using Contact.Api.Repository;
using Contact.Api.Service;
using DnsClient;
using DotNetCore.CAP.Dashboard.NodeDiscovery;
using IdentityServer4.Services;
using Luke.Infrastructure.Consul;
using Luke.Infrastructure.Consul.Options;
using Luke.Infrastructure.DotNetCoreCap;
using Luke.Infrastructure.Identity;
using Luke.Infrastructure.Identity.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Resilience;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;

namespace Contact.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddOptions();
            services.Configure<DBSettings>(Configuration.GetSection("ConnectionStrings"));//数据库连接信息
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));//其他配置信息 

            services.AddScoped<IContactApplyRequestRepository, MongoContactApplyRequestRepository>();
            services.AddScoped<IContactRepository, MongoContactRepository>();
            services.AddTransient<IRepository<LogEventData>, LogRepository>();
            services.AddScoped<IUserService, UserService>();

            services.AddIdentityServer()
            .AddDeveloperSigningCredential()
            .AddInMemoryClients(Config.GetClients())
            .AddInMemoryIdentityResources(Config.GetIdentityResources());

            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.AddMyDnsQuery();
            services.AddMyConsulClient();
            services.AddMyHttpClient();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            }) .AddJwtBearer(options =>
                {
                    options.Authority = "http://localhost:91";
                    options.Audience = "gateway_contactapi";
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                });

            services.Configure<UseCapDiscoveryOptions>(Configuration.GetSection("DotNetCapDiscovery"));
            services.AddMyDotNetCoreCap(options => {
          
                options.UseMySql(Configuration.GetConnectionString("MysqlConnection_beta_contact"));

            });
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime,
            IOptions<ServiceDisvoveryOptions> serviceOptions, IConsulClient consulClient)
        {
            app.UseMyConsul(applicationLifetime, serviceOptions, consulClient);


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();

            app.UseAuthorization();
            app.UseIdentityServer();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


    }
}
