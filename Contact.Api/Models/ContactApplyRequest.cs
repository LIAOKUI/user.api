﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contact.Api.Models
{
    public class ContactApplyRequest
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }
        /// <summary>
        /// 申请人
        /// </summary>
        public int  ApplierId { get; set; }
        /// <summary>
        /// 是否通过 0未通过 1 已通过
        /// </summary>
        public byte Approvaled { get; set; }
        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime? HandledTime { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime ApplyTime { get; set; }
    }
}
