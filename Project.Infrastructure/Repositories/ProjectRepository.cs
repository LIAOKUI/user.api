﻿using Microsoft.EntityFrameworkCore;
using Project.Domain.AggregatesModel;
using Project.Domain.SeedWork;
using System.Threading.Tasks;

namespace Project.Infrastructure.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ProjectDbContext dbContext;
        public ProjectRepository(ProjectDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IUnitOfWork UnitOfWork => this.dbContext;

        public async Task<Domain.AggregatesModel.Project> AddAsync(Domain.AggregatesModel.Project project)
        {
            var result = await this.dbContext.AddAsync(project);
            return result.Entity;
        }

        public async Task<Domain.AggregatesModel.Project> GetAsync(int id)
        {
            return await dbContext.Projects.Include(s => s.ProjectContriburors)
                 .Include(s => s.ProjectContriburors)
                 .Include(s => s.ProjectContriburors)
                 .SingleOrDefaultAsync(s => s.Id == id);
        }

        public async Task<Domain.AggregatesModel.Project> UpdateAsync(Domain.AggregatesModel.Project project)
        {
            var result = dbContext.Update(project);
            await Task.CompletedTask;
            return result.Entity;
        }
    }
}
