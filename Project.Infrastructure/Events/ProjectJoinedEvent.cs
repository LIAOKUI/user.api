﻿using MediatR;
using Project.Domain.AggregatesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Infrastructure.Events
{
   public  class ProjectJoinedEvent:INotification
    {
        public ProjectContributor ProjectContributor { get; set; }
    }
}
