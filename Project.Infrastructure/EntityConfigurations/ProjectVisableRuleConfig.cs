﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.Domain.AggregatesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Infrastructure.EntityConfigurations
{
    public class ProjectVisableRuleConfig : IEntityTypeConfiguration<ProjectVisableRule>
    {
        public void Configure(EntityTypeBuilder<ProjectVisableRule> builder)
        {
            builder.ToTable("ProjectVisableRules").HasKey(s=>s.Id);
        }
    }
}
