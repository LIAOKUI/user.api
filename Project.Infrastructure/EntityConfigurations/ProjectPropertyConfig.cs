﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.Domain.AggregatesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Infrastructure.EntityConfigurations
{
    public class ProjectPropertyConfig : IEntityTypeConfiguration<ProjectPropetry>
    {
        public void Configure(EntityTypeBuilder<ProjectPropetry> builder)
        {
            builder.ToTable("ProjectPropetrys");
            builder.Property(s=>s.Key).IsRequired().HasMaxLength(100);
            builder.Property(s => s.Value).IsRequired().HasMaxLength(100);

            builder.HasKey(s=>new { s.ProjecetId,s.Key,s.Value});
        }
    }
}
