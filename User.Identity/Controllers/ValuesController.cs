﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace User.Identity.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<UserIdentity> Get()
        {
            var http = HttpContext.User.Claims;
            var claims = User.Claims;
            var identity = new UserIdentity
            {
                UserId = Convert.ToInt32(claims.FirstOrDefault(s => s.Type == "sub").Value),
                Name = claims.FirstOrDefault(s => s.Type == "name").Value,
                Avatar = claims.FirstOrDefault(s => s.Type == "avatar").Value,
                Titile = claims.FirstOrDefault(s => s.Type == "title").Value,
                Company = claims.FirstOrDefault(s => s.Type == "cpmpany").Value
            };
            return identity;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class UserIdentity
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Titile { get; set; }
        public string Company { get; set; }
    }
}
