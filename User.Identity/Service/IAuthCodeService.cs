﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.Identity.Service
{
    public interface IAuthCodeService
    {
        /// <summary>
        /// 根据手机号和验证码验证
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="authCode"></param>
        /// <returns></returns>
        bool Validate(string phone,string authCode);
    }
}
