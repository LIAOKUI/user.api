﻿using DnsClient;
using Luke.Infrastructure.Consul.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using User.Identity.Dtos;


namespace User.Identity.Service
{
    public class UserService1 : IUserService

    {
        private HttpClient _httpClient;
        private readonly string _userServiceUrl;// = "http://localhost:91/";
        public UserService1(HttpClient httpClient, IOptions<ServiceDisvoveryOptions> serviceDisvoveryOptions, IDnsQuery dnsQuery)
        {
            _httpClient = httpClient;
            //基于服务发现获取地址
            var address = dnsQuery.ResolveService("server.consul", serviceDisvoveryOptions.Value.UserServiceName);
            var addressList = address.First().AddressList;
            var host = addressList.Any() ? addressList.First().ToString() : address.First().HostName;
            var port = address.First().Port;
            _userServiceUrl = $"http://{host}:{port}";
        }
        public async Task<UserInfo> CheckOrCreate(string phone)
        {
            var from = new Dictionary<string, string>
            {
                { "phone", phone }
            };
            var content = new FormUrlEncodedContent(from);

            //  content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"); 

            var response = await _httpClient.PostAsync(_userServiceUrl + "api/User/check-or-create", content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // int.TryParse(await response.Content.ReadAsStringAsync(), out int userId);
                var result = await response.Content.ReadAsStringAsync();
                var userInfo = JsonConvert.DeserializeObject<UserInfo>(result);
                return userInfo;
            }
            return null;


        }
    }
}
