﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luke.Infrastructure.Api.Core.Model
{
    public class UserOperationException : Exception
    {
        public UserOperationException() { }
        public UserOperationException(string message) : base(message) { }
        public UserOperationException(string message, Exception innerException)
        : base(message, innerException) { }
    }
}
