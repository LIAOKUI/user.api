﻿using Luke.Infrastructure.Api.Core.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Luke.Infrastructure.Api.Core.Filters
{
    public class GloableExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger<GloableExceptionFilter> _logger;

        public GloableExceptionFilter(IHostingEnvironment hostingEnvironment, ILogger<GloableExceptionFilter> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(UserOperationException))
            {

                var json = JsonErrorResponseMap(context.Exception.Message, context.Exception.StackTrace, context);
                context.Result = new BadRequestObjectResult(json);
            }
            else
            {
                var json = JsonErrorResponseMap("发生了未知内部错误", context.Exception.StackTrace, context);
                context.Result = new InternalServerErrorObjectResult(json);
            }
            _logger.LogError(context.Exception, context.Exception.Message);
            context.ExceptionHandled = true;
            _logger.LogError(context.Exception, context.Exception.Message);
        }
        private JsonErrorResponse JsonErrorResponseMap(string message, string devloperMessage, ExceptionContext context)
        {
            var json = new JsonErrorResponse();
            json.Messsage = message;
            if (_hostingEnvironment.IsDevelopment())
            {
                json.DevloperMessage = devloperMessage;
            }
            return json;
        }
    }

    public class InternalServerErrorObjectResult : ObjectResult
    {
        public InternalServerErrorObjectResult(object value) : base(value)
        {
            StatusCode = StatusCodes.Status500InternalServerError;
        }
    }
}
