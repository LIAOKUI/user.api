namespace User.Api.Dtos
{
    public class UserIdentity
    {

        /// <summary>
        /// 用户id
        /// </summary>
        /// <value></value>
        public int UserId{get;set;}
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name{get;set;}

        /// <summary>
        /// 头像
        /// </summary>
        /// <value></value>
        public string Avatar{get;set;}
    }
}