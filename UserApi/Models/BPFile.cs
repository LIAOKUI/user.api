﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.Api.Models
{
    public class BPFile
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        public int FileName { get; set; }
        /// <summary>
        /// 上传的源文件地址
        /// </summary>
        public int OriginFilePath { get; set; }
        /// <summary>
        /// 转换后的地址
        /// </summary>
        public int FromatFilePath { get; set; }
        public int CreateTime { get; set; }
    }
}
