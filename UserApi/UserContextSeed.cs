﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Api.Models;

namespace User.Api
{
    public class UserContextSeed
    {
        public static async Task SeedAsync(IApplicationBuilder ApplicationBuilder, ILoggerFactory LoggerFactory, int? retry = 0)
        {
            var retryForAvaibility = retry.Value;
            using var scope = ApplicationBuilder.ApplicationServices.CreateScope();
            try
            {
                //var context = (AppDbContext)scope.ServiceProvider.GetRequiredService(typeof(AppDbContext));
                var context = scope.ServiceProvider.GetRequiredService<UserDbContext>();
                var logger = (ILogger<UserContextSeed>)scope.ServiceProvider.GetService(typeof(ILogger<UserContextSeed>));
                logger.LogDebug("Begin UserContextSeed SeedAsyc");
                context.Database.Migrate();
                if (context.Users.Count() <= 0)
                {
                    context.Add(new Users { Name = "cyao", Company = "nocompany", Age = 1, Phone = "15511111111" });
                    context.SaveChanges();
                }
            }
            catch (System.Exception ex)
            {
                retryForAvaibility++;
                if (retryForAvaibility > 10)
                {
                    var logger = LoggerFactory.CreateLogger(typeof(UserContextSeed));
                    logger.LogError(ex.Message);
                    await SeedAsync(ApplicationBuilder, LoggerFactory, retryForAvaibility);
                }
            }
        }

        private void InitDataBase(IApplicationBuilder app)
        {

            using (var scope = app.ApplicationServices.CreateScope())
            {
                var userContext = scope.ServiceProvider.GetRequiredService<UserDbContext>();
                userContext.Database.Migrate();
                if (!userContext.Users.Any())
                {
                    userContext.Users.Add(new Models.Users() { Age = 66, Company = "myword", Name = "xiaohong", Phone = "15511111111" });
                    userContext.SaveChanges();
                }
            }
        }
    }
}
