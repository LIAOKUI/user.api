﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Api.Models;

namespace User.Api
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>()
                .HasKey(s => s.Id);
            modelBuilder.Entity<Users>()
               .HasIndex(s => s.Id);
            modelBuilder.Entity<UserProperty>().Property(s => s.Value).HasMaxLength(100);
            modelBuilder.Entity<UserProperty>().Property(s => s.Key).HasMaxLength(100);
            modelBuilder.Entity<UserProperty>().HasKey(s => new { s.Key, s.UserId, s.Value });

            modelBuilder.Entity<UserTag>()
               .HasKey(s => new { s.UserId, s.Tag });
            modelBuilder.Entity<UserTag>().Property(s => s.Tag).HasMaxLength(100);

            modelBuilder.Entity<BPFile>()
            .HasKey(s => s.Id);
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<BPFile> BPFile { get; set; }
        public DbSet<UserProperty> UserProperty { get; set; }
        public DbSet<UserTag> UserTag { get; set; }
    }
}
