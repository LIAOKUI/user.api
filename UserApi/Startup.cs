﻿using Consul;
using DotNetCore.CAP.Dashboard.NodeDiscovery;
using Luke.Infrastructure.Api.Core.Filters;
using Luke.Infrastructure.Commom;
using Luke.Infrastructure.Consul;
using Luke.Infrastructure.Consul.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Linq;
using User.Api.Dtos;

namespace User.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UserDbContext>(options =>
            {
                options.UseMySql(Configuration.GetConnectionString("MysqlConnection"));
                //options.UseSqlServer(Configuration.GetConnectionString("SqlConnection"));
            });

            services.AddControllers().AddNewtonsoftJson();
            //  services.AddOptions();
            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));

            services.AddMyConsulClient();
            services.AddAutoMapper();

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(GloableExceptionFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);


            #region swagger
            services.AddSwaggerGen(options =>
     {
         string contactName = Configuration.GetSection("SwaggerDoc:ContactName").Value;
         string contactNameEmail = Configuration.GetSection("SwaggerDoc:ContactEmail").Value;
         string contactUrl = Configuration.GetSection("SwaggerDoc:ContactUrl").Value;
         var version = Configuration.GetSection("SwaggerDoc:Version").Value;
         var title = Configuration.GetSection("SwaggerDoc:Title").Value;
         var description = Configuration.GetSection("SwaggerDoc:Description").Value;
         options.SwaggerDoc(version, new OpenApiInfo
         {
             Version = version,
             Title = title,
             Description = description,
             Contact = new OpenApiContact { Name = contactName, Email = contactNameEmail, Url = new Uri(contactUrl) },
             License = new OpenApiLicense { Name = contactName, Url = new Uri(contactUrl) }
         });

         var basePath = PlatformServices.Default.Application.ApplicationBasePath;
         var xmlPath = Path.Combine(basePath, "User.Api.xml");
         options.IncludeXmlComments(xmlPath);
         //options.DocumentFilter<HiddenApiFilter>(); // 在接口类、方法标记属性 [HiddenApi]，可以阻止【Swagger文档】生成
         //options.OperationFilter<AddHeaderOperationFilter>("correlationId", "Correlation Id for the request", false); // adds any string you like to the request headers - in this case, a correlation id
         // options.OperationFilter<AddResponseHeadersFilter>();
         //options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
         // options.OperationFilter<SecurityRequirementsOperationFilter>();
         //给api添加token令牌证书
         options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
         {
             Description = "JWT授权(数据将在请求头中进行传输) 直接在下框中输入Bearer {token}（注意两者之间是一个空格）\"",
             Name = "Authorization",
             In = ParameterLocation.Header,//jwt默认存放Authorization信息的位置(请求头中)
             Type = SecuritySchemeType.ApiKey
         });

       
     });
            #endregion

            #region cap
            services.AddCap(options =>
            {
                options.UseMySql(Configuration.GetConnectionString("MysqlConnection_beta_contact"));
                options.UseRabbitMQ((o) => {
                    o.HostName = "localhost";
                    o.Password = "guest";
                    o.UserName = "guest";
                });
                options.UseDashboard();
                options.UseDiscovery(d =>
                {
                    d.DiscoveryServerHostName = "localhost";
                    d.DiscoveryServerPort = 8500;
                    d.CurrentNodeHostName = "localhost";
                    d.CurrentNodePort = 94;
                    d.NodeId = "2";
                    d.NodeName = "CAP No.2 Node User.API";
                });
                options.SucceedMessageExpiredAfter = 24 * 3600;
                options.FailedRetryCount = 5;
            });

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime,
            IOptions<ServiceDisvoveryOptions> serviceOptions, IConsulClient consulClient)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            //启用中间件服务对swagger-ui，指定Swagger JSON终结点
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                // c.RoutePrefix = string.Empty;
            });
            //app.UseHttpsRedirection();
            app.UseMyConsul(applicationLifetime, serviceOptions, consulClient);

            UserContextSeed.SeedAsync(app, loggerFactory).Wait();
            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseStateAutoMapper();
       
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

    }
}
