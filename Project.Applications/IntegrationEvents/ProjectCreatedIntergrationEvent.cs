﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Applications.IntegrationEvents
{
 public    class ProjectCreatedIntergrationEvent:IntegrationEvent
    {
        public int UserId { get; set; }
        public string Company { get; set; }
        public int ProjectId { get; set; }
        public string Introduction { get; set; }
    }
}
