﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Applications.IntegrationEvents
{
   public class  IntegrationEvent
    {
        public IntegrationEvent()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }
        public Guid Id { get; set; }
        public DateTime CreationDate { get; set; }

    }
}
