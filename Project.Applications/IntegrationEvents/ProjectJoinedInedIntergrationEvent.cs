﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Applications.IntegrationEvents
{
   public  class ProjectJoinedInedIntergrationEvent:IntegrationEvent
    {
        public int ProjectId { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
    }
}
