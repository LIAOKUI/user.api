﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project.Applications.Services
{
    public interface ICommandService
    {
        /// <summary>
        /// 是否推荐项目
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        Task<bool> IsRecommandProject(int projectId, int userId);
    }
}
