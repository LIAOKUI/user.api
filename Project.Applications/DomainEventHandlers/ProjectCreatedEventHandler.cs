﻿using DotNetCore.CAP;
using Luke.Infrastructure.Commom.Helper;
using MediatR;
using Project.Applications.IntegrationEvents;
using Project.Domain.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Applications.DomainEventHandlers
{
    public class ProjectCreatedEventHandler : INotificationHandler<ProejctViewedEvent>
    {
        private readonly ICapPublisher capPublisher;
        public ProjectCreatedEventHandler(ICapPublisher capPublisher)
        {
            this.capPublisher = capPublisher;
        }
        public async Task Handle(ProejctViewedEvent notification, CancellationToken cancellationToken)
        {
            var result = new ProejctViewedIntergrationEvent
            {
                ProjectId = notification.ProjectViewer.ProjectId,
                UserId = notification.ProjectViewer.UserId,
                UserName = notification.ProjectViewer.UserName
            };
            await capPublisher.PublishAsync("projectapi.projectviewed", result);
        }
    }
}
