﻿using DotNetCore.CAP;
using MediatR;
using Project.Applications.IntegrationEvents;
using Project.Domain.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Applications.DomainEventHandlers
{
    public class ProjectViewEventHandler : INotificationHandler<ProjectJoinedEvent>
    {
        private readonly ICapPublisher capPublisher;
        public ProjectViewEventHandler(ICapPublisher capPublisher)
        {
            this.capPublisher = capPublisher;
        }
        public async Task Handle(ProjectJoinedEvent notification, CancellationToken cancellationToken)
        {
            var result = new ProjectJoinedInedIntergrationEvent()
            {
                ProjectId = notification.ProjectContributor.ProjectId,
                UserId = notification.ProjectContributor.UserId,
                UserName = notification.ProjectContributor.UserName
            };
            await capPublisher.PublishAsync("projecetapi.projectjoined", result);
        }
    }
}
