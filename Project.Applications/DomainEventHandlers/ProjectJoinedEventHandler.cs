﻿using DotNetCore.CAP;
using MediatR;
using Project.Applications.IntegrationEvents;
using Project.Domain.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Applications.DomainEventHandlers
{
    public class ProjectJoinedEventHandler : INotificationHandler<ProjectCreatedEvent>
    {
        private readonly ICapPublisher capPublisher;
        public ProjectJoinedEventHandler(ICapPublisher capPublisher)
        {
            this.capPublisher = capPublisher;
        }
        public async Task Handle(ProjectCreatedEvent notification, CancellationToken cancellationToken)
        {
            var result = new ProjectCreatedIntergrationEvent()
            {
                Company = notification.Project.Cpmpany,
                Introduction = notification.Project.Introduction,
                UserId = notification.Project.UserId,
                ProjectId = notification.Project.Id
            };
            await capPublisher.PublishAsync("projectapi.projectcreated", result);
        }
    }
}
