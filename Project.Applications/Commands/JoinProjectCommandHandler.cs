﻿using MediatR;
using Project.Domain.AggregatesModel;
using Project.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Applications.Commands
{
    public class JoinProjectCommandHandler : IRequestHandler<JoinProjecetCommand>
    {

        private readonly IProjectRepository projectRepository;
        public JoinProjectCommandHandler(IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
        }
        public async Task<Unit> Handle(JoinProjecetCommand request, CancellationToken cancellationToken)
        {
            var project = await projectRepository.GetAsync(request.ProjectContributor.ProjectId);
            if (project == null)
                throw new ProjectDomainException($"project not find with projecetId{request.ProjectContributor.ProjectId}");
            project.AddContributor(request.ProjectContributor);
            await projectRepository.UnitOfWork.SaveChangesAsync();
            return Unit.Value;
        }
    }
}
