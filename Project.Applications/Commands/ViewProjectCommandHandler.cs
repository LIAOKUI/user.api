﻿using MediatR;
using Project.Domain.AggregatesModel;
using Project.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project.Applications.Commands
{
    public class ViewProjectCommandHandler : IRequestHandler<ViewProjectCommand>
    {
        private readonly IProjectRepository projectRepository;
        public ViewProjectCommandHandler(IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        public async Task<Unit> Handle(ViewProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await projectRepository.GetAsync(request.ProjectViewer.ProjectId);
            if (project == null)
                throw new ProjectDomainException($"project not find with projecetId:{request.ProjectViewer.ProjectId}");
            project.AddViewer(request.ProjectViewer.UserName, request.ProjectViewer.Avator,
                request.ProjectViewer.UserId);
            await projectRepository.UnitOfWork.SaveChangesAsync();
            return Unit.Value;
        }
    }
}
