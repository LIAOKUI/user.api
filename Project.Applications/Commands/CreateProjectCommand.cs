﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Applications.Commands
{
    public class CreateProjectCommand : IRequest
    {
        public Domain.AggregatesModel.Project Project { get; set; }
    }
}
