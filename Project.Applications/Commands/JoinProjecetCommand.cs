﻿using MediatR;
using Project.Domain.AggregatesModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project.Applications.Commands
{
   public  class JoinProjecetCommand:IRequest
    {
       public ProjectContributor ProjectContributor { get; set; }
    }
}
