﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Project.Infrastructure;

namespace Project.Applications.Queries
{
    /// <summary>
    /// 项目查询
    /// </summary>
    public class ProjectQueries : IProjectQueries
    {
        private readonly string connection;
        private readonly ProjectDbContext dbContext;
        public ProjectQueries(string connection, ProjectDbContext dbContext)
        {
            this.connection = connection;
            this.dbContext = dbContext;
        }
        /// <summary>
        /// 获取项目详情信息
        /// </summary>
        public async System.Threading.Tasks.Task<dynamic> GetProjectDetailAsync(int projectId)
        {
            var proejct = await dbContext.Projects.Include(s => s.ProjectContriburors)
                .Include(s => s.ProjectPropetries)
                .Include(s => s.ProjectViewers)
                   .Include(s => s.ProjectVisableRule).FirstOrDefaultAsync(s => s.Id == projectId);
            return proejct;
        }
        /// <summary>
        /// 获取当前用户项目
        /// </summary>
        public async System.Threading.Tasks.Task<dynamic> GetProjectListByUserIdAsync(int userId)
        {
            var result = await dbContext.Projects.FirstOrDefaultAsync(s => s.UserId == userId);
            return result;
        }
    }
}
