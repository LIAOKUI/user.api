﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project.Applications.Queries
{
  public   interface IProjectQueries
    {
        Task<dynamic> GetProjectListByUserIdAsync(int userId);
        Task<dynamic> GetProjectDetailAsync(int projectId);
    }
}
