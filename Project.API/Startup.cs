using Consul;
using Luke.Infrastructure.Consul;
using Luke.Infrastructure.Consul.Options;
using Luke.Infrastructure.DotNetCoreCap;
using Luke.Infrastructure.Identity;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Project.Applications.Queries;
using Project.Applications.Services;
using Project.Domain.AggregatesModel;
using Project.Infrastructure;
using Project.Infrastructure.Repositories;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;

namespace Project.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<ProjectDbContext>(builder =>
            {
                builder.UseMySql(Configuration.GetConnectionString("UserMysqlLocal"), x =>
                 {
                     x.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                 });
            });

            services.AddOptions();
            services.Configure<ServiceDisvoveryOptions>(Configuration.GetSection("ServiceDiscovery"));
            services.Configure<UseCapDiscoveryOptions>(Configuration.GetSection("DotNetCapDiscovery"));

            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ICommandService, TestCommandService>();
            services.AddScoped<IProjectQueries>(sp => new ProjectQueries(Configuration.GetConnectionString("UserMysqlLocal"), sp.GetRequiredService<ProjectDbContext>()));

            services.AddIdentityServer()
           .AddDeveloperSigningCredential()
           .AddInMemoryClients(Config.GetClients())
          .AddInMemoryIdentityResources(Config.GetIdentityResources());

            services.AddMediatR();
            services.AddMyDnsQuery();
            services.AddMyConsulClient();
          

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "http://localhost:91";
                options.Audience = "gateway_projectapi";
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;

            });

            services.AddMyDotNetCoreCap(options =>
            {
               // options.UseMySql(Configuration.GetConnectionString("UserMysqlLocal"));
                 options.UseEntityFramework<ProjectDbContext>();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime,
            IOptions<ServiceDisvoveryOptions> serviceOptions, IConsulClient consulClient)
        {
            app.UseMyConsul(applicationLifetime, serviceOptions, consulClient);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();

            app.UseAuthorization();
            app.UseIdentityServer();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
