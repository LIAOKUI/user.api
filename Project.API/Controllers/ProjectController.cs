﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Project.Applications.Commands;
using Project.Applications.Queries;
using Project.Applications.Services;
using Project.Domain.AggregatesModel;

namespace Project.API.Controllers
{
    public class ProjectController : BaseController
    {
        private readonly IMediator mediator;
        private readonly ICommandService commandService;
        private readonly IProjectQueries projectQueries;
        public ProjectController(IMediator mediator, IProjectQueries projectQueries, ICommandService commandService)
        {
            this.mediator = mediator;
            this.projectQueries = projectQueries;
            this.commandService = commandService;
        }
        /// <summary>
        /// 获取用户项目列表
        /// </summary>
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetProjectListByUserId(int userId)
        {
            var result = await projectQueries.GetProjectListByUserIdAsync(userId);
            return Ok(result);
        }
        /// <summary>
        /// 获取当前用户项目列表
        /// </summary>
        [HttpGet("my")]
        public async Task<IActionResult> GetMyProjectList()
        {

            var result = await projectQueries.GetProjectListByUserIdAsync(UserIdentity.UserId);
            return Ok(result);
        }

        /// <summary>
        /// 获取项目详情信息
        /// </summary>
        [HttpGet("detail/{projectId}")]
        public async Task<IActionResult> GetProjectDetail(int projectId)
        {
            var result = await projectQueries.GetProjectDetailAsync(projectId);
            return Ok(result);
        }

        /// <summary>
        /// 查看推荐项目信息
        /// </summary>
        public async Task<IActionResult> RecommandProjectDetail(int projectId)
        {
            var b = await commandService.IsRecommandProject(projectId, UserIdentity.UserId);
            if (!b) return BadRequest("无权查看此项目");
            var result = await projectQueries.GetProjectDetailAsync(projectId);
            return Ok(result);
        }
        /// <summary>
        /// 添加项目
        /// </summary>
        [HttpPost("add")]
        public async Task<IActionResult> AddProject(Domain.AggregatesModel.Project project)
        {
            if (project == null)
                throw new ArgumentNullException(nameof(project));
            var cmd = new CreateProjectCommand() { Project = project };
            cmd.Project.UserId = UserIdentity.UserId;
            await mediator.Send(cmd);
            return Ok();
        }

        /// <summary>
        /// 查看项目
        /// </summary>
        [HttpPut("view/{proejctId}")]
        public async Task<IActionResult> ViewProject(int projectId)
        {
            if (!await commandService.IsRecommandProject(projectId, UserIdentity.UserId))
                return BadRequest("不具有查看当前项目的权限");
            var cmd = new ViewProjectCommand()
            {
                ProjectViewer = new ProjectViewer()
                {
                    Avator = UserIdentity.Avatar,
                    CreateTime = DateTime.Now,
                    ProjectId = projectId,
                    UserId = UserIdentity.UserId,
                    UserName = UserIdentity.Name
                }
            };
            await mediator.Send(cmd);
            return Ok();
        }
        /// <summary>
        /// 加入项目
        /// </summary>
        [HttpPut("join")]
        public async Task<IActionResult> JoinProject(ProjectContributor contributor)
        {
            if (contributor == null)
                throw new ArgumentNullException(nameof(contributor));
            if (!await commandService.IsRecommandProject(contributor.ProjectId, UserIdentity.UserId))
                return BadRequest("不具有查看当前项目的权限");
            var cmd = new JoinProjecetCommand() { ProjectContributor = contributor };
            await mediator.Send(cmd);
            return Ok();
        }
    }
}