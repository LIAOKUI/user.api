﻿using Luke.Infrastructure.Consul.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Luke.Infrastructure.Consul.Options
{
    public class ServiceDisvoveryOptions
    {
        public string UserServiceName { get; set; }
        public string ServiceName { get; set; }
        public string ContactServiceName { get; set; }
        public ConsulOptions Consul { get; set; }
    }
}
